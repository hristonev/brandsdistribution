Brandsdistribution API bundle
=============================

## Usage
``` php
require BASE_DIR. '/vendor/autoload.php';

use Parser\XMLReader;
$xml = new XMLReader(BASE_DIR. '/path-to-writeble-folder'); // Download and state files!
$xml->setCredentials(
    'https://www.brandsdistribution.com/restful/export/api/products.xml', // endpoint
    'username',
    'password'
);
$xml->download();
$xml->parse();
while ($node = $xml->getItem()){
    print_r($node);die; // Dump first node of collection.
}
```